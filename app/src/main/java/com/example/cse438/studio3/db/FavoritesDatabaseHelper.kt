package com.example.cse438.studio3.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class FavoritesDatabaseHelper(context: Context): SQLiteOpenHelper(context, DbSettings.DB_NAME, null, DbSettings.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createFavoritesTableQuery = "CREATE TABLE " + DbSettings.DBFavoriteEntry.TABLE + " ( " +
                DbSettings.DBFavoriteEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBFavoriteEntry.COL_API_ID + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_FAV_TITLE + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_DESCRIPTION + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_MANUFACTURER + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_MODEL + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_BRAND + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_CATEGORY_ID + " INTEGER NULL, " +
                DbSettings.DBFavoriteEntry.COL_CATEGORY_NAME + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_WEIGHT + " REAL NULL, " +
                DbSettings.DBFavoriteEntry.COL_LENGTH + " REAL NULL, " +
                DbSettings.DBFavoriteEntry.COL_WIDTH + " REAL NULL, " +
                DbSettings.DBFavoriteEntry.COL_HEIGHT + " REAL NULL, " +
                DbSettings.DBFavoriteEntry.COL_IMAGE_COUNT + " INTEGER NULL, " +
                DbSettings.DBFavoriteEntry.COL_PRICE + " REAL NULL, " +
                DbSettings.DBFavoriteEntry.COL_CURRENCY + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_COLOR + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_IS_NEW + " INTEGER NULL, " +
                DbSettings.DBFavoriteEntry.COL_UPC + " INTEGER NULL, " +
                DbSettings.DBFavoriteEntry.COL_EAN + " INTEGER NULL, " +
                DbSettings.DBFavoriteEntry.COL_MPN + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_CREATED_DATE + " INTEGER NULL, " +
                DbSettings.DBFavoriteEntry.COL_UPDATED_DATE + " INTEGER NULL, " +
                DbSettings.DBFavoriteEntry.COL_SEM3_ID + " TEXT NULL);"

        val createImageAssetTableQuery = "CREATE TABLE " + DbSettings.DBImageAssetEntry.TABLE + " ( " +
                //TODO: Insert code to set up the three columns in the images table
                "CONSTRAINT fk_favorites FOREIGN KEY(" + DbSettings.DBImageAssetEntry.FAVORITE_ID + ") " +
                "REFERENCES " + DbSettings.DBFavoriteEntry.TABLE + "(" + DbSettings.DBFavoriteEntry.ID + ") ON DELETE CASCADE);"

        val createFeatureTableQuery = "CREATE TABLE " + DbSettings.DBFeatureEntry.TABLE + " ( " +
                DbSettings.DBFeatureEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBFeatureEntry.FAVORITE_ID + " INTEGER NOT NULL, " +
                DbSettings.DBFeatureEntry.COL_NAME + " TEXT NULL, " +
                DbSettings.DBFeatureEntry.COL_DESCRIPTION + " TEXT NULL, " +
                " FOREIGN KEY(" + DbSettings.DBFeatureEntry.FAVORITE_ID + ") " +
                "REFERENCES " + DbSettings.DBFavoriteEntry.TABLE + "(" + DbSettings.DBFavoriteEntry.ID + ") ON DELETE CASCADE);"

        val createGtinTableEntry = "CREATE TABLE " + DbSettings.DBGtinEntry.TABLE + " ( " +
                DbSettings.DBGtinEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBGtinEntry.FAVORITE_ID + " INTEGER NOT NULL, " +
                DbSettings.DBGtinEntry.COL_GTIN + " INTEGER NULL, " +
                " FOREIGN KEY(" + DbSettings.DBGtinEntry.FAVORITE_ID + ") " +
                "REFERENCES " + DbSettings.DBFavoriteEntry.TABLE + "(" + DbSettings.DBFavoriteEntry.ID + ") ON DELETE CASCADE);"

        val createVarSecIdTableEntry = "CREATE TABLE " + DbSettings.DBVariationSecondaryIdEntry.TABLE + " ( " +
                DbSettings.DBVariationSecondaryIdEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBVariationSecondaryIdEntry.FAVORITE_ID + " INTEGER NOT NULL, " +
                DbSettings.DBVariationSecondaryIdEntry.COL_VAR_SEC_ID + " TEXT NULL, " +
                " FOREIGN KEY(" + DbSettings.DBVariationSecondaryIdEntry.FAVORITE_ID + ") " +
                "REFERENCES " + DbSettings.DBFavoriteEntry.TABLE + "(" + DbSettings.DBFavoriteEntry.ID + ") ON DELETE CASCADE);"

        val createGeoTableEntry = "CREATE TABLE " + DbSettings.DBGeographyEntry.TABLE + " ( " +
                DbSettings.DBGeographyEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBGeographyEntry.FAVORITE_ID + " INTEGER NOT NULL, " +
                DbSettings.DBGeographyEntry.COL_GEO + " TEXT NULL, " +
                " FOREIGN KEY(" + DbSettings.DBGeographyEntry.FAVORITE_ID + ") " +
                "REFERENCES " + DbSettings.DBFavoriteEntry.TABLE + "(" + DbSettings.DBFavoriteEntry.ID + ") ON DELETE CASCADE);"

        val createMessageTableEntry = "CREATE TABLE " + DbSettings.DBMessageEntry.TABLE + " ( " +
                DbSettings.DBMessageEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBMessageEntry.FAVORITE_ID + " INTEGER NOT NULL, " +
                DbSettings.DBMessageEntry.COL_MESSAGE + " TEXT NULL, " +
                " FOREIGN KEY(" + DbSettings.DBMessageEntry.FAVORITE_ID + ") " +
                "REFERENCES " + DbSettings.DBFavoriteEntry.TABLE + "(" + DbSettings.DBFavoriteEntry.ID + ") ON DELETE CASCADE);"

        val createSiteDetailTableEntry = "CREATE TABLE " + DbSettings.DBSiteDetailEntry.TABLE + " ( " +
                DbSettings.DBSiteDetailEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBSiteDetailEntry.FAVORITE_ID + " INTEGER NOT NULL, " +
                DbSettings.DBSiteDetailEntry.COL_NAME + " TEXT NULL, " +
                DbSettings.DBSiteDetailEntry.COL_URL + " TEXT NULL, " +
                DbSettings.DBSiteDetailEntry.COL_SKU + " TEXT NULL, " +
                DbSettings.DBSiteDetailEntry.COL_OFFERS_COUNT + " INTEGER NULL, " +
                " FOREIGN KEY(" + DbSettings.DBSiteDetailEntry.FAVORITE_ID + ") " +
                "REFERENCES " + DbSettings.DBFavoriteEntry.TABLE + "(" + DbSettings.DBFavoriteEntry.ID + ") ON DELETE CASCADE);"

        val createOfferTableEntry = "CREATE TABLE " + DbSettings.DBOfferEntry.TABLE + " ( " +
                DbSettings.DBOfferEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBOfferEntry.SITE_DETAIL_ID + " INTEGER NOT NULL, " +
                DbSettings.DBOfferEntry.COL_API_ID + " TEXT NULL, " +
                DbSettings.DBOfferEntry.COL_PRICE + " REAL NULL, " +
                DbSettings.DBOfferEntry.COL_CURRENCY + " TEXT NULL, " +
                DbSettings.DBOfferEntry.COL_SELLER + " TEXT NULL, " +
                DbSettings.DBOfferEntry.COL_CONDITION + " TEXT NULL, " +
                DbSettings.DBOfferEntry.COL_AVAILABILITY + " TEXT NULL, " +
                DbSettings.DBOfferEntry.COL_IS_ACTIVE + " INTEGER NULL, " +
                DbSettings.DBOfferEntry.COL_FIRST_RECORDED + " INTEGER NULL, " +
                DbSettings.DBOfferEntry.COL_LAST_RECORDED + " INTEGER NULL, " +
                " FOREIGN KEY(" + DbSettings.DBOfferEntry.SITE_DETAIL_ID + ") " +
                "REFERENCES " + DbSettings.DBSiteDetailEntry.TABLE + "(" + DbSettings.DBSiteDetailEntry.ID + ") ON DELETE CASCADE);"

        db?.execSQL(createFavoritesTableQuery)
        db?.execSQL(createImageAssetTableQuery)
        db?.execSQL(createFeatureTableQuery)
        db?.execSQL(createGtinTableEntry)
        db?.execSQL(createVarSecIdTableEntry)
        db?.execSQL(createGeoTableEntry)
        db?.execSQL(createMessageTableEntry)
        db?.execSQL(createSiteDetailTableEntry)
        db?.execSQL(createOfferTableEntry)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + DbSettings.DBFavoriteEntry.TABLE)
        onCreate(db)
    }
}